import db from './db.json';

export const API_URL = process.env.API_URL || '';

export function saveUserPlace() {
    return Promise.resolve();
}

export function deleteUserPlace() {
    return Promise.resolve();
}

export function getUsers() {
    return Promise.resolve(db.result);
}

export function getAuthData() {
    return Promise.resolve({slackId: 'U1ECBKKFS'});
}

export function logout() {
    return Promise.resolve();
}
