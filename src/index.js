import Viewer from './viewer/app';
import Assets from './assets';
import store from './store';
import * as Actions from './actions';

const assets = new Assets();
const viewer = new Viewer({assets, store});

function setCanvasSize(w, h) {
    store.dispatch(Actions.setCanvasSize(w, h));
}

export default {
    assets,
    viewer,
    actions: {setCanvasSize},
    setCanvasSize,
    init(element) {
        element.appendChild(viewer.domElement);

        setCanvasSize(element.clientWidth, element.clientHeight);

        return Promise.resolve();
    }
};
