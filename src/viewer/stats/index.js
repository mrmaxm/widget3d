import StatsJS from 'stats.js';
import reduce from 'lodash/reduce';
import isEqual from 'lodash/isEqual';

import './index.css';

function rendInfoTmpl(info) {
    return reduce(info, (template, val, name) => {
        return template + `<div class="stats-item">${name}: <span class="stats-value">${val}</span></div>`;
    }, '');
}

export default class Stats {
    constructor({renderer, isEnabled = true} = {}) {
        const stats = new StatsJS();

        Object.assign(this, {
            isEnabled,
            renderer,
            _stats: stats,
            _infoDomElement: null,
            info: null,
            prevInfo: {}
        });

        const infoEl = document.createElement('div');
        infoEl.id = 'stats-info';
        stats.domElement.appendChild(infoEl);
        this._infoDomElement = infoEl;

        document.body.appendChild(stats.domElement);

        this.animate = this.animate.bind(this);
    }

    _updateInfo() {
        const info = this.renderer.info;
        const curInfo = {};

        curInfo.geometries = info.memory.geometries;
        curInfo.textures = info.memory.textures;
        curInfo.programs = info.programs.length;
        curInfo.drawcalls = info.render.calls;
        curInfo.faces = info.render.triangles;

        if (isEqual(curInfo, this.prevInfo)) { return; }

        this._infoDomElement.innerHTML = rendInfoTmpl(curInfo);

        Object.assign(this.prevInfo, curInfo);
        this.info = curInfo;
    }

    animate() {
        if (!this.isEnabled) { return; }

        const stats = this._stats;

        this._updateInfo();
        stats.update();
    }
}
