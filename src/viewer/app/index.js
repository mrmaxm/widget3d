import pick from 'lodash/pick';
import {bindActionCreators} from 'redux';
import * as THREE from 'three';

import * as Actions from '../../actions';

import config from './config';

import Gl from '../gl';
import Env from '../env';
import Views from '../views';
import Controls from '../controls';
import Raycastser from '../raycaster';
import Material from '../material';
import Postprocess from '../postprocess';

export default class Viewer {
    constructor({assets, store}) {
        const state = store.getState();
        const actions = bindActionCreators(Actions, store.dispatch);
        if (!state.isWebglSupported) { return; }

        this.actions = actions;
        this.config = config;
        this.assets = assets;
        this.store = store;
        this.state = state;

        this.env = new Env({config});

        this.views = new Views({
            config,
            assets,
            onLoad: (...args) => {
                if (state.isDebug) {
                    this.debugReady.then(() => {
                        this._handleLoad(...args);
                    });
                    return;
                }

                this._handleLoad(...args);
            },
            env: this.env
        });
        this.env.scene.add(this.views.container);
        // this.views.container.rotation.fromArray([-0.20538257057366682, -2.7460410361886547, 0]);

        this.gl = new Gl({config: config.renderer, scene: this.env.scene});

        this.controls = new Controls({
            object: this.views.container,
            config: pick(config, ['camera', 'controls']),
            domElement: this.gl.domElement
        });
        this.gl.camera = this.controls.camera;
        this.env.setCamera(this.controls.camera);

        const raycaster = new Raycastser(this.gl.camera, this.gl.renderer.domElement);
        this.raycaster = raycaster;

        this.material = new Material({
            anisotropy: Math.min(4, this.gl.renderer.capabilities.getMaxAnisotropy()),
            isDebug: state.isDebug,
            onChange: () => {
                this.gl.runOnce();
            }
        });

        this.postprocess = new Postprocess({
            renderer: this.gl.renderer,
            scene: this.env.scene,
            camera: this.controls.camera
        });

        // Object.assign(this.controls, {
        //     onStartUpdate: raycaster.disable,
        //     onFinishUpdate: raycaster.enable,
        //     onUpdate: this._updatePopupPosition.bind(this)
        // });

        this.views.show(config.startView);
        this.domElement = this.gl.domElement;

        this.gl.setMainLoop(() => {
            // this.gl.render();
        });
        this.gl.setConstLoop(() => {
            // this.tweening.animate();
            this.controls.animate();
            this.postprocess.render();
            // this.env.animate();
        });
        this.gl.runConstLoop();

        if (state.isDebug) {
            window.THREE = THREE;

            this.debugReady = new Promise((resolve) => {
                /* eslint-disable import/no-unresolved, global-require */
                require.ensure([], () => {
                    const Stats = require('../stats').default;
                    this.gui = require('../debug-gui').create(this.gl.domElement, this.controls.camera, this.env.scene);
                    this.gui.update();

                    this.stats = new Stats({renderer: this.gl.renderer});
                    this.gl.addAdditionalLoop(this.stats.animate);

                    resolve();
                }, 'debug');
                /* eslint-enable */
            });
        }

        this._size = [0, 0];

        this._addListeners();
    }

    _addListeners() {
        if (!this.state.isDebug) {
            this.controls.onStartUpdate = () => {
                // this.raycaster.disable();
                // this.raycasterIcons.disable();
                this.gl.run();
            };

            this.controls.onFinishUpdate = () => {
                // this.raycaster.enable();
                // this.raycasterIcons.enable();
                this.gl.stop();
            };
        }

        this.store.subscribe(() => {
            const state = this.store.getState();

            this._updateCanvasSize(...state.canvasSize);

            if (!state.isModelLoaded) { return; }

            // console.log('state', state);

            this.gl.runOnce();

            this.state = state;
        });
    }

    _handleLoad(view, meshes, perModelAssets) {
        this.actions.loadModel();

        const RAYCASTER_TAG = 'RACASTING';

        this.raycaster.addSetup(RAYCASTER_TAG, {
            objects: meshes,
            onHover: (/* id */) => {
                // console.log('hover', id);
            },
            onUnhover: (/* id */) => {
                // console.log('unhover', id);
                this.controls.resetZoomTarget();
            },
            onMove: (intersect) => {
                this.controls.setZoomTarget(intersect.point);
            },
            onClick: () => {}
        });
        this.raycaster.enableSetup(RAYCASTER_TAG);

        perModelAssets.forEach((modelAssets) => {
            this.material.set(modelAssets.object3d, modelAssets, modelAssets.settings);
        });

        if (this.state.isDebug) {
            this.gl.run();
            this.gui.update();

            /* eslint-disable import/no-unresolved, global-require */
            // require.ensure([], () => {
            //     require('./testPlaces/index').test(this.places._collidersByPlace);
            // }, 'debug');
            /* eslint-enable */
        }
    }

    _updateCanvasSize(width, height) {
        if (width === this.state.canvasSize.width &&
            height === this.state.canvasSize.height) {
            return;
        }

        this.gl.updateSize(width, height);
        this.postprocess.updateSize(width, height);
        this.controls.updateRatio(width, height);

        this._size[0] = width;
        this._size[1] = height;
    }

    _get2dCoords(object3d, {offsetY = 0} = {}) {
        object3d.getWorldPosition((this._screenVector));
        this._screenVector.y += offsetY;
        this._screenVector.project(this.controls.camera);

        return [
            Math.round(((this._screenVector.x + 1) * this._size[0]) / 2),
            Math.round(((1 - this._screenVector.y) * this._size[1]) / 2)
        ];
    }
}
