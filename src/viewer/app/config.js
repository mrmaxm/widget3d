const devicePixelRatio = window.devicePixelRatio || 1;
const isSafariBrowser = navigator.userAgent.includes('Safari') && !navigator.userAgent.includes('Chrome');

export default {
    renderer: {
        clearColor: 0x000000,
        alpha: true,
        antialias: devicePixelRatio === 1 || !isSafariBrowser
    },

    camera: {
        // position: [37.78190575327407, 141.8294774077805, -92.25011221457379]
        position: [0, 30, 152]
    },

    controls: {
        screenSpacePanning: true,
        minDistance: 30,
        maxDistance: 150,
        zoomSpeed: 100,
        panSpeed: 0.5,
        // rotateSpeed: 0.25,
        // enableDamping: true,
        minPolarAngle: (-15 * Math.PI) / 180,
        maxPolarAngle: (30 * Math.PI) / 180,
        // enableKeys: false,
        // target: {x: 0, y: 125, z: 0}
    }
};
