/* eslint-disable class-methods-use-this */
import * as THREE from 'three';

import get from 'lodash/get';
import template from 'lodash/template';
import templateSettings from 'lodash/templateSettings';

import TexturesGUI from './debug';

import fragShaderTemplate from './resources/phong.frag';
import vertShaderTemplate from './resources/phong.vert';

const bakTemplate = templateSettings.interpolate;
templateSettings.interpolate = /#include <([\s\S]+?)>/g;
const fragShader = /* '#extension GL_OES_standard_derivatives : enable \n' +  */template(fragShaderTemplate)(THREE.ShaderChunk);
const vertShader = template(vertShaderTemplate)(THREE.ShaderChunk);
templateSettings.interpolate = bakTemplate;

export default class Material {
    constructor({onChange, isDebug, anisotropy}) {
        this._onChange = onChange;
        this._anisotropy = anisotropy;

        this._material = this._createMaterial();

        if (isDebug) {
            /* eslint-disable import/no-unresolved, global-require */
            require.ensure([], () => {
                this._gui = new TexturesGUI(this._material, onChange);
            }, 'debug');
            /* eslint-enable */
        }
    }

    _createMaterial() {
        const material = new THREE.ShaderMaterial({
            uniforms: THREE.UniformsUtils.merge([
                {diffuse: {}},
                {diffuse2: {value: new THREE.Color(0.0, 0.0, 0.0)}},
                {diffuse2Mix: {value: 0.2}},
                {emissive: {}},
                {specular: {}},
                {shininess: {}},
                {diffuseUVRepeat: {value: 1}},
                {otherUVRepeat: {value: 1}},
                {aoMapIntensity: {}},
                {bumpScale: {value: 0.99}},
                {normalScale: {}},
                THREE.ShaderLib.phong.uniforms,
                {rampMap: {}}
            ]),
            vertexShader: vertShader,
            fragmentShader: fragShader,
            lights: true,
            name: 'custom-material',
            defines: {
                USE_MAP: true
                // USE_NORMALMAP: true,
                // USE_AOMAP: true,
                // USE_BUMPMAP: true,
                // USE_SPECULARMAP: true
            }
        });

        // material.map = true;
        material.aoMap = true;
        material.bumpMap = true;
        material.normalMap = true;
        material.specularMap = true;

        material.uniforms.bumpScale.value = 0.99;
        material.uniforms.aoMapIntensity.value = 0.99;

        return material;
    }

    set(wrapper3d, assets, settings) {
        const material = this._material.clone();

        wrapper3d.traverse((object3d) => {
            if (!(object3d instanceof THREE.Mesh)) { return; }

            object3d.scale.set(1, 1, 1);

            const object3dSettings = settings[object3d.name];

            if (!object3dSettings) { return; }

            const parts = wrapper3d.name.split('/');
            const name = parts[parts.length - 1];

            const {type, ...materialSettings} = object3dSettings;

            if (type) {
                object3d.material = new THREE[type]();
            } else {
                object3d.material = material;
            }

            object3d.name = `${name}_${object3d.name}`;
            object3d.material.name = object3d.material.name || object3d.name;

            Object.entries(materialSettings).forEach(([materialKey, materialValue]) => {
                if (typeof materialValue === 'string' && materialValue.startsWith('$')) {
                    const tx = get(assets, materialValue.replace('$', ''));
                    tx.anisotropy = this._anisotropy;

                    if (object3d.material instanceof THREE.ShaderMaterial) {
                        object3d.material.uniforms[materialKey].value = tx;

                        if (materialKey !== 'map') {
                            object3d.material[materialKey] = true;
                        }
                    } else {
                        object3d.material[materialKey] = tx;
                    }

                    tx.needsUpdate = true;
                } else if (typeof materialValue === 'string' && materialValue.startsWith('#')) {
                    const color = new THREE.Color().setStyle(materialValue);
                    if (object3d.material instanceof THREE.ShaderMaterial) {
                        object3d.material.uniforms[materialKey].value = color;
                    } else {
                        object3d.material[materialKey] = color;
                    }
                } else if (Array.isArray(materialValue)) {
                    if (object3d.material instanceof THREE.ShaderMaterial) {
                        object3d.material.uniforms[materialKey].value.fromArray(materialValue);
                    } else {
                        object3d.material[materialKey].fromArray(materialValue);
                    }
                } else {
                    // eslint-disable-next-line no-lonely-if
                    if (object3d.material instanceof THREE.ShaderMaterial) {
                        object3d.material.uniforms[materialKey].value = materialValue;
                    } else {
                        object3d.material[materialKey] = materialValue;
                    }
                }

                if (this._gui && object3d.material instanceof THREE.ShaderMaterial) {
                    this._gui.updateTexture(materialKey, material);
                }
            });

            if (this._gui && object3d.material instanceof THREE.ShaderMaterial) {
                this._gui.addModelTextures(wrapper3d.name, material);
            }

            object3d.material.needsUpdate = true;
        });

        // Test sphere
        // const geometry = new THREE.SphereBufferGeometry(15, 32, 32);
        // const sphere = new THREE.Mesh(geometry, material);
        // sphere.position.set(0, 170, 0);
        // wrapper3d.add(sphere);

        this._onChange();
    }
}
