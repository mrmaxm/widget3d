import * as THREE from 'three';
import throttle from 'lodash/throttle';

const CLICK_TIMEOUT = 200;

export default class Raycaster {
    constructor(camera, canvas) {
        this.camera = camera;
        this.canvas = canvas;

        this.isEnabled = true;
        this._isClickTimeExpired = false;
        this._clickTimerId = null;

        this._tempVector = new THREE.Vector3();
        this._raycaster = new THREE.Raycaster();
        this._setups = {};
        this._setup = null;

        this._objects = [];
        this._isIgnoringObjectsById = {};
        this._hoveredMesh = null;
        this._onHover = () => {};
        this._onUnhover = () => {};
        this._onMove = () => {};
        this._onClick = () => {};
        this._onMissClick = () => {};

        this._handleMove = throttle(this._handleMove.bind(this), 10);
        this._handleStart = this._handleStart.bind(this);
        this._handleEnd = this._handleEnd.bind(this);
        this.disable = this.disable.bind(this);
        this.enable = this.enable.bind(this);

        this._addListeners();
    }

    dispose() {
        // TODO
        clearTimeout(this._clickTimerId);
        this._removeListeners();
    }

    enable() {
        this.isEnabled = true;
    }

    disable() {
        this.isEnabled = false;
    }

    addSetup(setupName, {objects, additionalObjects = [], onHover, onUnhover, onClick, onMissClick, onMove}) {
        if (!objects || !onHover || !onUnhover || !onClick) {
            throw new Error('Smth not passed');
        }

        this._setups[setupName] = {
            objects: [...objects, ...additionalObjects],
            isIgnoringObjectsById: additionalObjects.reduce((byId, object) => {
                byId[object.id] = true;

                return byId;
            }, {}),
            onHover,
            onUnhover,
            onClick,
            onMissClick,
            onMove
        };
    }

    enableSetup(setupName) {
        const setup = this._setups[setupName];
        if (!setup) { throw new Error(`Setup '${setupName}' not found`); }

        this._setup = setupName;
        this._objects = setup.objects;
        this._isIgnoringObjectsById = setup.isIgnoringObjectsById;
        this._onHover = setup.onHover || this._onHover;
        this._onUnhover = setup.onUnhover || this._onUnhover;
        this._onMove = setup.onMove || this._onMove;
        this._onClick = setup.onClick || this._onClick;
        this._onMissClick = setup.onMissClick || this._onMissClick;
        this._hoveredMesh = null;
    }

    disableSetup(setupName) {
        const setup = this._setups[setupName];
        if (!setup) { throw new Error(`Setup '${setupName}' not found`); }
        if (this._setup !== setupName) { return; }

        this._objects = [];
        this._onHover = () => {};
        this._onUnhover = () => {};
        this._onMove = () => {};
        this._onClick = () => {};
        this._hoveredMesh = null;
    }

    _addListeners() {
        const canvas = this.canvas;

        canvas.addEventListener('touchstart', this._handleStart);
        canvas.addEventListener('touchend', this._handleEnd);
        canvas.addEventListener('touchmove', this._handleMove);

        canvas.addEventListener('mousedown', this._handleStart);
        canvas.addEventListener('mouseup', this._handleEnd);
        canvas.addEventListener('mousemove', this._handleMove);
    }

    _removeListeners() {
        const canvas = this.canvas;

        canvas.removeEventListener('touchstart', this._handleStart);
        canvas.removeEventListener('touchend', this._handleEnd);
        canvas.removeEventListener('touchmove', this._handleMove);

        canvas.removeEventListener('mousedown', this._handleStart);
        canvas.removeEventListener('mouseup', this._handleEnd);
        canvas.removeEventListener('mousemove', this._handleMove);
    }

    _handleStart() {
        if (!this.isEnabled) { return; }

        clearTimeout(this._clickTimerId);

        // устанавливаем таймер для клика
        // если в течение CLICK_TIMEOUT не сработает _handleEnd, значит это не клик
        this._isClickTimeExpired = false;
        this._clickTimerId = setTimeout(() => {
            this._isClickTimeExpired = true;
        }, CLICK_TIMEOUT);
    }

    _handleEnd(event) {
        if (!this.isEnabled) { return; }

        clearTimeout(this._clickTimerId);
        if (this._isClickTimeExpired) { return; }

        const coords = this._getCoordsFromEvent(event);
        const intersects = this._getIntersects(coords);

        if (!intersects.length) {
            this._onMissClick();
            return;
        }

        const intersect = intersects[0];

        if (this._isIgnoringObjectsById[intersect.object.id]) {
            this._onMissClick();
            return;
        }

        this._onClick(intersect.object, intersect);
    }

    _handleMove(event) {
        if (!this.isEnabled) { return; }

        const coords = this._getCoordsFromEvent(event);
        const intersects = this._getIntersects(coords);

        if (!intersects.length || this._isIgnoringObjectsById[intersects[0].object.id]) {
            if (this._hoveredMesh) {
                this._hoveredMesh = null;
                this._onUnhover();
            }
            return;
        }

        const intersect = intersects[0];

        this._onMove(intersect);

        if (this._hoveredMesh === intersect.object) { return; }

        this._hoveredMesh = intersect.object;

        this._onHover(intersect.object, intersect);
    }

    _getCoordsFromEvent(event) {
        const x = event.touches ? event.touches[0].pageX : event.clientX;
        const y = event.touches ? event.touches[0].pageY : event.clientY;

        return {
            x: ((2 * x) / this.canvas.clientWidth) - 1,
            y: ((-2 * y) / this.canvas.clientHeight) + 1
        };
    }

    _getIntersects(coords) {
        this._tempVector.set(coords.x, coords.y, 1);
        this._tempVector.unproject(this.camera);
        this._raycaster.set(this.camera.position, this._tempVector.sub(this.camera.position).normalize());

        return this._raycaster.intersectObjects(this._objects);
    }
}
