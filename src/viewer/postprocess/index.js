import * as THREE from 'three';
import 'three/examples/js/shaders/CopyShader';
import 'three/examples/js/shaders/BokehShader';
import 'three/examples/js/postprocessing/EffectComposer';
import 'three/examples/js/postprocessing/RenderPass';
import 'three/examples/js/postprocessing/ShaderPass';
import 'three/examples/js/postprocessing/MaskPass';
import 'three/examples/js/postprocessing/BokehPass';
import * as dat from 'dat-gui';

export default class Postprocess {
    constructor({renderer, scene, camera}) {
        renderer.autoClear = false;

        const {width, height} = renderer.getSize();

        const renderPass = new THREE.RenderPass(scene, camera);
        const bokehPass = new THREE.BokehPass(scene, camera, {
            focus: 500,
            aperture: 2 * 0.00001,
            maxblur: 0.02,
            width,
            height
        });

        bokehPass.renderToScreen = true;
        const composer = new THREE.EffectComposer(renderer);

        composer.addPass(renderPass);
        composer.addPass(bokehPass);

        const gui = new dat.GUI();

        gui.add(bokehPass.uniforms.focus, 'value').name('focus');
        gui.add(bokehPass.uniforms.aperture, 'value', 0, 10 * 0.00001).name('aperture');
        gui.add(bokehPass.uniforms.maxblur, 'value', 0, 0.1).name('maxblur');


        this._composer = composer;
        this._bokeh = bokehPass;
    }

    updateSize(w, h) {
        this._composer.setSize(w, h);
    }

    render() {
        this._composer.render(0.1);
    }
}
