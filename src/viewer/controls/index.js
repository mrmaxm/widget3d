import * as THREE from 'three';

import defaults from 'lodash/defaults';
import debounce from 'lodash/debounce';
import merge from 'lodash/merge';

import OrbitControls from './OrbitControls';

const defaultCameraConfig = {
    fov: 35,
    minDistance: 1,
    maxDistance: 7000,
    position: [0, 0, 100]
};

const noop = f => f;

export default class Controls {
    constructor({config = {}, object, domElement, onStartUpdate = noop, onFinishUpdate = noop, onUpdate = noop} = {}) {
        config.camera = defaults(config.camera || {}, defaultCameraConfig);

        const {fov, aspectRatio, minDistance, maxDistance, position} = config.camera;
        const camera = new THREE.PerspectiveCamera(fov, aspectRatio, minDistance, maxDistance);
        camera.position.fromArray(position);

        const _controls = new OrbitControls(object, camera, domElement);
        _controls.camera = camera;

        Object.assign(this, {
            config,
            camera,
            object,
            onStartUpdate,
            onFinishUpdate,
            onUpdate,
            clock: new THREE.Clock(),
            _controls,
            target: _controls.target,
            zoomTarget: _controls.target
        });

        this._handleStart = debounce(() => this.onStartUpdate(), 300, {leading: true, trailing: false});
        this._handleFinish = debounce(() => this.onFinishUpdate(), 300);
        this._handleUpdate = () => this.onUpdate();


        this.setConfig(this.config.controls);
        this._addListeners();
    }

    setZoomTarget(target) {
        this._controls.zoomTarget = target;
        // this._controls.target = target;
    }

    resetZoomTarget() {
        this._controls.zoomTarget = this._controls.target;
    }

    animate() {
        this._controls.update(this.clock.getDelta());
    }

    updateRatio(w, h) {
        this.camera.aspect = w / h;
        this.camera.updateProjectionMatrix();
    }

    setConfig(opts) {
        merge(this._controls, opts, (a, b) => (a instanceof THREE.Vector3 ? a.fromArray(b) : undefined));
    }

    _addListeners() {
        const controls = this._controls;

        controls.addEventListener('change', this._handleStart);
        controls.addEventListener('change', this._handleFinish);
        controls.addEventListener('change', this._handleUpdate);

        // Max distance event
        // const distanceBias = 0.1;
        // eventhub.on('b3app:controls:end', () => {
        //     const distance = this.camera.position.distanceTo(controls.target);

        //     if (distance < controls.maxDistance - distanceBias) { return; }

        //     eventhub.fire('b3app:controls:maxDistance');
        // });
    }
}
