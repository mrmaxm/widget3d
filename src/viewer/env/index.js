import * as THREE from 'three';

// const tempVector = new THREE.Vector3();

export default class Env {
    constructor({config}) {
        const scene = new THREE.Scene();
        scene.name = 'Scene';

        const lights = new THREE.Group();
        lights.name = 'Lights';
        // lights.position.set(0, -125, 0);

        const ambientLight = new THREE.AmbientLight(0xffffff, 1.8);
        ambientLight.name = 'ambientLight';

        const directionalLightLeft = new THREE.DirectionalLight(0xffffff, 0.01);
        directionalLightLeft.name = 'directionalLightLeft';
        directionalLightLeft.position.set(-279, 182, 81);
        directionalLightLeft.target.position.set(-23, 147, -97);

        const directionalLightRight = new THREE.DirectionalLight(0xffffff, 0.01);
        directionalLightRight.name = 'directionalLightRight';
        directionalLightRight.position.set(155, 246, 193);
        directionalLightRight.target.position.set(-17, 143, 50);

        scene.add(lights);
        lights.add(ambientLight);
        lights.add(directionalLightRight);
        lights.add(directionalLightLeft);

        this.config = config;
        this.scene = scene;
        this.camera = null;
        this.lights = lights;

        this._frustum = new THREE.Frustum();
        this._matrix = new THREE.Matrix4();
    }

    setCamera(camera) {
        this.camera = camera;

        this.scene.add(camera);
    }

    // animate() {
        // this.camera.getWorldDirection(tempVector);

        // // Rotate lights with camera
        // this.lights.rotation.y = Math.atan2(tempVector.x, tempVector.z) + Math.PI;
    // }

    isInFrustum(object) {
        if (!object) { return false; }

        const camera = this.camera;

        camera.updateMatrix();
        camera.updateMatrixWorld();
        camera.matrixWorldInverse.getInverse(camera.matrixWorld);

        object.updateMatrix();
        object.updateMatrixWorld();

        this._matrix.multiplyMatrices(camera.projectionMatrix, camera.matrixWorldInverse);
        this._frustum.setFromMatrix(this._matrix);

        return this._frustum.intersectsObject(object);
    }
}
