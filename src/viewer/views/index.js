import * as THREE from 'three';

export default class Views {
    constructor({config, assets, onLoad, env, container = new THREE.Object3D()}) {
        container.name = container.name || 'Models';

        Object.assign(this, {
            config,
            assets,
            onLoad,
            env,
            container,
            currentView: null,
            data: {}
        });
    }

    show(id) {
        const loadingView = id ? this.assets.getById('view', id) : this.assets.getFirst('view');

        return loadingView
            .then((view) => {
                this.currentView = view;

                const loadingAssets = view.models.map((modelInfo) => {
                    const texturesByName = {};
                    const assets = {
                        object3d: null,
                        textures: texturesByName,
                        settings: modelInfo.settings,
                        position: modelInfo.position
                    };

                    const loadingTextures = Object.keys(modelInfo.textures)
                        .map((textureName) => {
                            const textureUrl = modelInfo.textures[textureName];

                            return this.assets.getById('texture', textureUrl)
                                .then((texture) => {
                                    texturesByName[textureName] = texture;
                                });
                        });

                    return Promise.all([
                        this.assets.getById('model', modelInfo.object3d),
                        Promise.all(loadingTextures)
                    ]).then(([object3d]) => {
                        assets.object3d = object3d;
                        return assets;
                    });
                });

                return Promise.all(loadingAssets);
            })
            .then((perModelAssets) => {
                this._clearContainer();

                perModelAssets.forEach((modelAssets) => {
                    this.container.add(modelAssets.object3d);
                    modelAssets.object3d.scale.set(1, 1, 1);
                    modelAssets.object3d.position.y = -125;

                    if (modelAssets.position) {
                        modelAssets.object3d.position.fromArray(modelAssets.position);
                    }
                });

                const meshes = [];
                this.container.traverse((object3d) => {
                    object3d.scale.set(1, 1, 1);

                    if (object3d instanceof THREE.Light) {
                        object3d.parent.remove(object3d);
                    }

                    if (!object3d.material) { return; }

                    meshes.push(object3d);
                });

                this.onLoad(this.currentView, meshes, perModelAssets);
            })
            .catch(e => console.error(e)); // eslint-disable-line no-console
    }

    _clearContainer() {
        while (this.container.children.length) {
            this.container.remove(this.container.children[0]);
        }
    }
}
