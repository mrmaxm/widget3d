import ViewType from './types/view';
import ModelType from './types/model';
import TextureType from './types/texture';

export default function(assets) {
    const {basePath = ''} = assets.config;
    const types = assets._types;

    types.view = new ViewType({basePath});
    types.model = new ModelType({basePath});
    types.texture = new TextureType({basePath});
}
