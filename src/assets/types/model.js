import path from 'path';
import * as THREE from 'three';
import 'three/examples/js/loaders/LoaderSupport';
import 'three/examples/js/loaders/GLTFLoader';
import template from 'lodash/template';

const loaderGLTF = new THREE.GLTFLoader();

// const loader = new THREE.OBJLoader2();
// loader.meshBuilder.setLogging(false);
// loader.logging.enabled = false;

export default class ModelType {
    constructor({basePath}) {
        const dirPath = path.join(basePath, 'assets/models/');
        this._template = `${dirPath}<%= id %>`;
    }

    getById(id) {
        const src = template(this._template)({id});

        return new Promise((resolve, reject) => {
            loaderGLTF.load(src, ({scene}) => {
                const wrapper = new THREE.Group();
                wrapper.name = id;

                // scene.detail.loaderRootNode.children.forEach((child) => {
                //     wrapper.add(child);
                // });
                // const wrapper = scene.detail.loaderRootNode.clone();

                [...scene.children].forEach(child => wrapper.add(child));

                resolve(wrapper);
            }, undefined, reject);

            // loaderOBJ.load(src, (scene) => {
            //     const wrapper = new THREE.Group();
            //     wrapper.name = id;

            //     // scene.detail.loaderRootNode.children.forEach((child) => {
            //     //     wrapper.add(child);
            //     // });
            //     // const wrapper = scene.detail.loaderRootNode.clone();

            //     [...scene.children].forEach(child => wrapper.add(child));

            //     resolve(wrapper);
            // }, undefined, reject);
        });
    }
}
