/* eslint-disable class-methods-use-this */

const reqView = require.context('../../../app/assets/views/', false, /\.json$/);

export default class ViewType {
    getById(id) {
        const item = reqView('./' + id + '.json');
        return Promise.resolve(item);
    }

    getFirst() {
        const items = reqView('./_list.json');
        return this.getById(items[0].id);
    }
}
