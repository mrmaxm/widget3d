import * as THREE from 'three';
import path from 'path';
import template from 'lodash/template';

const loader = new THREE.TextureLoader();

export default class TextureType {
    constructor({basePath}) {
        const dirPath = path.join(basePath, 'assets/models/');
        this._template = `${dirPath}<%= id %>`;
    }

    getById(id) {
        if (typeof id === 'object') {
            id = id.src; // eslint-disable-line
        }

        return new Promise((resolve, reject) => {
            const finalSrc = template(this._template)({id});

            loader.load(finalSrc,
                (tx) => {
                    tx.wrapS = THREE.RepeatWrapping;
                    tx.wrapT = THREE.RepeatWrapping;
                    tx.needsUpdate = true;

                    resolve(tx);
                },
                undefined, // progress callback
                (err) => { reject(err); }
            );
        });
    }
}
