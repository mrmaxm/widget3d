import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers';

let defToolsExtensionMiddleware = f => f;
if (process.env.NODE_ENV === 'development' && window.devToolsExtension) {
    defToolsExtensionMiddleware = window.devToolsExtension();
}

const store = createStore(reducers, compose(
    applyMiddleware(thunk),
    defToolsExtensionMiddleware
));

export default store;
