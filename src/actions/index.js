import * as Config from '../action-types';

export function setCanvasSize(width, height) {
    return {
        type: Config.SET_CANVAS_SIZE,
        width,
        height
    };
}

export function loadModel() {
    return {
        type: Config.LOAD_MODEL
    };
}
