/* global clothesWidget3d */
/* eslint-disable */

const widget3d = clothesWidget3d.default;

widget3d.init(document.getElementById('app')).then(function() {
    window.addEventListener('resize', function() {
        widget3d.setCanvasSize(window.innerWidth, window.innerHeight);
    });
}).catch(function(error) {
    console.error(error);
});
