/* eslint-disable no-param-reassign */

const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const GulpSSH = require('gulp-ssh');

const $ = gulpLoadPlugins();

const del = require('del');

const webpack = require('./gulp/webpack')(gulp);

gulp.task('clean', cb => del(['dist/*', '!dist/.keep'], cb));

gulp.task('copy:assets', ['clean'], () => {
    return gulp.src('app/assets/**/*')
        .pipe(gulp.dest('dist/assets'));
});

gulp.task('textures', ['copy:assets'], () => {
    return gulp.src('dist/assets/**/*.*(png|jpeg|jpg)')
        .pipe($.imageResize({
            width: 1024,
            height: 1024,
            upscale: false
        }))
        .pipe($.imagemin({
            progressive: true,
            interlaced: true
        })
        .on('error', (err) => {
            console.log(err); // eslint-disable-line no-console
            this.end();
        }))
        .pipe(gulp.dest('dist/assets'));
});

gulp.task('assets', ['clean', 'copy:assets'/* , 'textures' */]);

gulp.task('extras', ['clean'], () => {
    return gulp.src([
        'app/**/*.*',
        '!app/assets/**/*.*',
        '!app/*.*(html|tmpl)'
    ], {
        dot: true
    }).pipe(gulp.dest('dist'));
});

gulp.task('serve', cb => webpack.run(cb, {watch: true}));

gulp.task('gzip', ['assets', 'webpack:prod'], () => {
    return gulp.src('dist/**/*.*(js|json|dae|glb|gltf|obj|bin|mem|data)')
       .pipe($.gzip({gzipOptions: {level: 9}}))
       .pipe(gulp.dest('dist'));
});

gulp.task('webpack', ['clean'], cb => webpack.run(cb));
gulp.task('webpack:prod', ['clean'], cb => webpack.run(cb, {prod: true}));
gulp.task('webpack:stats', ['clean'], cb => webpack.run(cb, {prod: true, stats: true}));

gulp.task('build', ['clean', 'extras', 'webpack:prod', 'assets', 'gzip']);

const gulpSSH = new GulpSSH({
    ignoreErrors: true,
    sshConfig: {
        host: '194.87.239.90',
        username: 'user',
        password: '13261427'
    }
});

gulp.task('deploy', ['build'], () => {
    return gulp.src(['dist/**/*.*'], {dot: true})
        .pipe(gulpSSH.dest('/var/www/html/webgl_test/dof/'));
});

gulp.task('deploy2', () => {
    return gulp.src(['dist/**/*.*'], {dot: true})
        .pipe(gulpSSH.dest('/var/www/html/webgl_test/4igoom/'));
});
