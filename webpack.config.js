/* eslint-disable no-param-reassign */

const path = require('path');
const webpack = require('webpack');
// const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const srcRoot = path.join(__dirname, 'src');

const uglifyPlugin = new UglifyJsPlugin();

const cssLoaders = [
    {
        loader: 'css-loader',
        query: { // not 'options': https://github.com/webpack/extract-text-webpack-plugin/issues/282
            sourceMap: true,
            modules: true,
            localIdentName: '[local]-[hash:base64:5]',
            context: __dirname
        }
    },
    {
        loader: 'postcss-loader'
    }
];

module.exports = (opts) => {
    opts = opts || {};

    const outputPostfix = opts.prod ? '.[hash]' : '';

    const plugins = [
        new webpack.ProvidePlugin({
            THREE: 'three'
        }),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(opts.prod ? 'production' : 'development'),
                'API_URL': JSON.stringify(process.env.API_URL || '')
            }
        })
    ].concat(opts.prod ? [
        // new ExtractTextPlugin(`styles/app${outputPostfix}.css`),
        uglifyPlugin,
        new HtmlWebpackPlugin({
            path: 'dist',
            template: '../app/index.tmpl',
            filename: 'index.html',
            inject: false
        })
    ] : [
        new webpack.HotModuleReplacementPlugin()
    ]);

    return {
        context: path.join(__dirname, 'src'),
        entry: [
            'url-search-params-polyfill',
            'babel-polyfill',
            'whatwg-fetch',
            './index.js'
        ],
        output: {
            path: path.join(__dirname, 'dist'),
            publicPath: '',
            filename: `scripts/app${outputPostfix}.js`,
            library: 'clothesWidget3d',
            libraryTarget: 'umd',
            chunkFilename: `scripts/[name]${outputPostfix}.js`
        },
        resolve: {
            alias: {
                'three/examples': 'three/examples/js',
                'three/chunks': 'three/src/renderers/shaders/ShaderChunk'
            }
        },
        plugins,
        module: {
            // preLoaders: [{
            //     test: /\.jsx?$/,
            //     loader: 'eslint-loader',
            //     include: srcRoot,
            // }],
            rules: [{
                test: /\.jsx?$/,
                use: 'babel-loader',
                include: [srcRoot, path.join(__dirname, 'node_modules/@avito/ui/src')],
                exclude: ['lodash', 'dat-gui'].map(name => RegExp(name))
            }, {
                test: /\.css$/,
                loader: opts.prod ? [
                    'style-loader',
                    ...cssLoaders
                ]
                /* ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: cssLoaders,
                    publicPath: '../'
                }) */ : [
                    'webpack-module-hot-accept',
                    'style-loader',
                    ...cssLoaders
                ]
            }, {
                test: /\.(glsl|vert|frag)$/,
                use: 'raw-loader'
            }, {
                test: /\.json$/,
                use: 'json-loader'
            }, {
                test: /\.(gif|jpe?g|png|svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 10000, // 10 KB
                            name: 'images/[name]_[hash:base64:5].[ext]'
                        }
                    }
                ]
            }]
        },
        // http://webpack.github.io/docs/build-performance.html#sourcemaps
        devtool: opts.prod ? undefined : 'cheap-source-map',
        performance: {
            hints: false
        }
    };
};
