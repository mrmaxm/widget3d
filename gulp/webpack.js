/* eslint-disable no-param-reassign */

const WebpackDevServer = require('webpack-dev-server');
const webpack = require('webpack');
const gutil = require('gulp-util');
const webpackConfig = require('../webpack.config.js');
const fs = require('fs');

const PORT = process.env.PORT || 9000;

function runServerOnPort(config, port) {
    const localServer = `http://localhost:${port}`;
    console.log(`Watching ${localServer}...`);

    config.entry.unshift(
        `webpack-dev-server/client?${localServer}`,
        'webpack/hot/dev-server'
    );
    config.output.publicPath = localServer + '/';

    let compiledConfig;
    try {
        compiledConfig = webpack(config);
    } catch (error) {
        console.log(error.message); // eslint-disable-line no-console
    }

    const server = new WebpackDevServer(compiledConfig, {
        hot: true,
        contentBase: 'app',
        stats: {
            colors: true,
            chunkModules: false,
            chunks: false,
            hash: false,
            version: false
        }
    });

    server.listen(port, (err) => {
        if (err) { throw new gutil.PluginError('webpack', err); }

        gutil.log('Listening', gutil.colors.magenta(localServer));
    });
}

function webpackTask(cb, opts) {
    opts = opts || {};
    cb = cb || function() {};

    const config = webpackConfig(opts);

    if (opts.watch) {
        return runServerOnPort(config, PORT);
    }

    webpack(config, (err, stats) => {
        if (err) { throw new gutil.PluginError('webpack', err); }

        console.log(stats.toString({
            colors: true,
            hash: false,
            chunks: false,
            timings: false,
            children: false
        }));

        if (opts.stats) {
            const json = JSON.stringify(stats.toJson(), null, 4) + '\n';
            fs.writeFile('stats.json', json, cb);
            return;
        }

        cb();
    });
}

module.exports = (/* gulp */) => {
    return {run: webpackTask};
};
